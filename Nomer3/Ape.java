package Nomer3;

/**
 * Created by Dima1 on 17.04.2016.
 */
public class Ape extends Animal {
    //вызов конструктора родителя
    public Ape(String name, int iq, String gender, int ves) {
        super(name, iq, gender, ves);
    }

    //  всякие методы
    public void getInfoApe() {
        System.out.println("Обезьяну зовут " + getName() + ". У неё IQ " + getIq() + ". Она весит " + getVes() + " кг. Пол " + getGender());
    }

    public void topHuman() {
        System.out.println("Обезьяна завоёвывает Землю.");
    }

    public void eatFish() {
        System.out.println("Обезьяна ест рыбу.");
    }

    public void metodApe(Fish fish, Human human) {
        if (fish.getIq() >= this.getIq() && fish.getIq() <= human.getIq()) fish.freakIq();
        else if (fish.getIq() >= human.getIq() && fish.getIq() >= this.getIq()) fish.over9000Iq();
        else if (this.getIq() >= human.getIq()) this.topHuman();
        else if (human.getIq() >= this.getIq()) human.tameApe();
        else if (human.getIq() <= this.getIq() && human.getIq() <= fish.getIq())
            System.out.println("У человека мозгов, как у хлебушка :D");
    }
}
