package Nomer3;

/**
 * Created by Dima1 on 17.04.2016.
 */
public class Fish extends Animal {
    //вызов конструктора родителя
    public Fish(String name, int iq, String gender, int ves) {
        super(name, iq, gender, ves);
    }

    //  всякие методы
    public void eatAll() {
        System.out.println("Рыба съедает всех");
    }

    public void eatHuman() {
        System.out.println("Рыба съедает человека");
    }

    public void eatApe() {
        System.out.println("Рыба съедает обезьяну");
    }

    public void freakIq() {
        System.out.println("Это странно, что у рыбы IQ больше, чем у обезьяны");
    }

    //  метод полной инфы о рыбе
    public void getInfo() {
        System.out.println("Рыбку зовут " + getName() + ". У неё IQ " + getIq() + ". Она весит " + getVes() + " кг. Её пол " + getGender());
    }

    public void over9000Iq() {
        System.out.println("Похоже, что это не рыба вовсе, а дельфин!");
    }

    public void metodFish(Ape ape, Human human) {
        if (this.getVes() >= human.getVes() && this.getVes() >= ape.getVes()) this.eatAll();
        else if (this.getVes() >= ape.getVes()) this.eatApe();
        else if (this.getVes() >= human.getVes()) this.eatHuman();
        else {
            human.fishing();
            ape.eatFish();
        }
    }
}
