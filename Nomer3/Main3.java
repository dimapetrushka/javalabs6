package Nomer3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dima1 on 18.04.2016.
 * Создать четыре класса "Animal", "Ape", "Fish", "Human"
 * Придумать им поля и методы
 * Супер-класс-"Animal", от него - "Fish" и "Ape", от "Ape" - "Human"
 */
public class Main3 {
    public static void main(String[] args) throws IOException {
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));

//      создаем объект "РЫБА", выводим всю инфу о ней
        System.out.println("Введите имя рыбы, IQ, пол и вес");
        String name = buf.readLine(), iq1 = buf.readLine(), gender = buf.readLine(), ves1 = buf.readLine();
        int iq = Integer.parseInt(iq1), ves = Integer.parseInt(ves1);
        Fish fish = new Fish(name, iq, gender, ves);
        fish.getInfo();

//      создаем объект "ОБЕЗЬЯНА", выводим инфу
        System.out.println("Введите имя обезьяны, IQ, пол и вес");
        name = buf.readLine();
        iq1 = buf.readLine();
        gender = buf.readLine();
        ves1 = buf.readLine();
        iq = Integer.parseInt(iq1);
        ves = Integer.parseInt(ves1);
        Ape ape = new Ape(name, iq, gender, ves);
        ape.getInfoApe();

//      создаем объект "ЧЕЛОВЕК", выводим инфу
        System.out.println("Введите имя человека, IQ, пол и вес");
        name = buf.readLine();
        iq1 = buf.readLine();
        gender = buf.readLine();
        ves1 = buf.readLine();
        iq = Integer.parseInt(iq1);
        ves = Integer.parseInt(ves1);
        Human human = new Human(name, iq, gender, ves);
        human.getInfoHuman();

        fish.metodFish(ape, human);
        ape.metodApe(fish, human);
    }
}
