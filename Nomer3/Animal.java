package Nomer3;

/**
 * Created by Dima1 on 17.04.2016.
 */
public class Animal {
    //создаем приватные переменные
    private String name;
    private int iq;
    private String gender;
    private int ves;

    //  создаем "получатели"
    public int getVes() {
        return this.ves;
    }

    public String getName() {
        return this.name;
    }

    public int getIq() {
        return this.iq;
    }

    //  конструктор
    public Animal(String name, int iq, String gender, int ves) {
        this.name = name;
        this.iq = iq;
        this.gender = gender;
        this.ves = ves;
    }

    public String getGender() {
        return this.gender;

    }
}
