package Nomer3;

/**
 * Created by Dima1 on 18.04.2016.
 */
public class Human extends Ape {
    //вызов конструктора родителя
    public Human(String name, int iq, String gender, int ves) {
        super(name, iq, gender, ves);
    }

    //  всякие методы
    public void getInfoHuman() {
        System.out.println("Человека зовут " + getName() + ". У него IQ " + getIq() + ". Весит " + getVes() + " кг. Пол " + getGender());
    }

    public void fishing() {
        System.out.println("Человек ловит рыбу");
    }

    public void tameApe() {
        System.out.println("Человек приручил обезьяну");
    }
}
