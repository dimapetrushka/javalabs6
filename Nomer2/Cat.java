package Nomer2;

/**
 * Created by Dima1 on 17.04.2016.
 */
public class Cat extends Pet {
    //создаем приватное имя
    private String name;
//  наследование конструктора от "домашнего животного"
    public Cat(String gender, int ves, int age) {
        super(gender, ves, age);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
//  возращает полную инфу он коте
    void getInfo() {
        System.out.println("\nКота зовут " + getName() + ". Он\\Она " + getGender() + ". Весит " + getVes() + " кг. Живёт на Земле уже " + getAge() + " лет.");
    }
}
