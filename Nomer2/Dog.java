package Nomer2;

/**
 * Created by Dima1 on 17.04.2016.
 */
public class Dog extends Pet {
    private String name;

    public Dog(String gender, int ves, int age) {
        super(gender, ves, age);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    void getInfo() {
        System.out.println("\nСобаку зовут " + getName() + ". Он\\Она " + getGender() + ". Весит " + getVes() + " кг. Живёт на Земле уже " + getAge() + " лет.");
    }
}
