package Nomer2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dima1 on 17.04.2016.
 * создается главный класс "Pet", от которого наследуются два класса "ДОГ" и "КОТ"
 * У наследников создать два метода "Name" и "getInfo"
 */
public class Main2 {
    public static void main(String[] args) throws IOException {
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите имя, пол, вес и возраст кота");
        String name = buf.readLine(), gender = buf.readLine(), ves1 = buf.readLine(), age1 = buf.readLine();
        int ves = Integer.parseInt(ves1), age = Integer.parseInt(age1);
        Cat cat = new Cat(gender, ves, age);
        cat.setName(name);

        System.out.println("Введите имя, пол, вес и возраст собаки");
        name = buf.readLine();
        gender = buf.readLine();
        ves1 = buf.readLine();
        age1 = buf.readLine();
        ves = Integer.parseInt(ves1);
        age = Integer.parseInt(age1);
        Dog dog = new Dog(gender, ves, age);
        dog.setName(name);

        cat.getInfo();
        dog.getInfo();
    }
}
