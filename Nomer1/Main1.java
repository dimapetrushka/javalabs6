package Nomer1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dima1 on 13.04.2016.
 * Класс реализующий принцип наследования
 * создается два объекта, один из которых наследуется от другого
 */
public class Main1 {
    public static void main(String[] args) throws IOException {
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите имя лошади");
        String name = buf.readLine();
        Horse horse = new Horse(name);
        System.out.println("Введите имя лошади");
        name = buf.readLine();
        Pegas pegas = new Pegas(name);
        pegas.fly();
    }
}
