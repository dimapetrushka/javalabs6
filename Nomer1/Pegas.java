package Nomer1;

/**
 * Created by Dima1 on 13.04.2016.
 */
public class Pegas extends Horse {
    //наследуем конструктор "лошади"
    public Pegas(String name) {
        super(name);
    }
//  метод полёта
    void fly() {
        System.out.println("Лошадь " + getName() + " летит");
    }
}
